<?php

namespace App\Http\Controllers;

use App\Models\BlogPost;
use App\Models\User;
use App\Models\Task;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class BlogPostController extends Controller
{
    
    public function index()
    {
        $posts = BlogPost::all(); //fetch all blog posts from DB

        return view('blog.index', [
                'posts' => $posts,
            ]); //returns the view with posts

    }

    public function create()
    {
        return view('blog.create');
    }

   
    public function store(Request $request)
    {
        $newPost = BlogPost::create([
            'title' => $request->title,
            'body' => strip_tags($request->body),
            'latitude'=> $request -> latitude,
            'longitude' => $request -> longitude,
            'user_id' => Auth::id()
        ]);

        $postID = $newPost -> id;

        if(!empty($request->task_name))
        {
            for($count = 0; $count < count($request->task_name); $count++)
            {
                Task::create([
                    'blog_post_id' => $postID,
                    'name' => $request -> task_name[$count],
                    'is_checked' => $request -> checkbox[$count]
                ]);
            }
        }

        return redirect('blog/' . $newPost->id);
    }

    public function show(BlogPost $blogPost)
    {
        $tasks = Task::all()->where('blog_post_id',$blogPost->id);  // fetch all tasks who are assigned to the post

        return view('blog.show', [
            'post' => $blogPost,
            'tasks' => $tasks
        ]); //returns the view with the post
    }

    
    public function edit(BlogPost $blogPost)
    {
        if (Auth::check() && (Auth::user()->id == $blogPost -> user_id || Auth::check() && Auth::user()->permission_level <= 1))
        {
            $tasks = Task::all()->where('blog_post_id',$blogPost->id);

            return view('blog.edit', [
                'post' => $blogPost,
                'tasks' => $tasks
            ]); // returns the edit post view with the selected post
        }
        else
        {
            return back();
        }
    }

    
    public function update(Request $request, BlogPost $blogPost)
    {
        $blogPost->update([
            'title' => $request->title,
            'body' => strip_tags($request->body),
            'latitude'=> $request -> latitude,
            'longitude' => $request -> longitude
        ]);

        $postID = $blogPost -> id;

        for($count = 0; $count < count($request->task_name); $count++)
        {
            if($request -> delete_task[$count] == 1) // checks if the delete button has been pressed for an existing task
            {
                Task::where('id',$request->task_id[$count])->delete(); // Deletes the appropriate task from the database
            }
            else
            {
                Task::updateOrCreate( // updates the tasks associated with the post
                [
                    'id' => $request -> task_id[$count], // attempts to find a task with the associated id, if no such tasks exist, a task is created instead
                ],
                [
                    'blog_post_id' => $postID,
                    'name' => $request -> task_name[$count],
                    'is_checked' => $request -> checkbox[$count]
                    // assigns the FK (blog_post_id) to the task to which it's associated to, and assigns the entered task name (name)
                ]);
            }
        }

        return redirect('blog/' . $blogPost->id);
    }

    
    public function destroy(BlogPost $blogPost)
    {
        $blogPost->delete();

        return redirect('/blog');
    }

    public function admin()
    {
        if(Auth::check() && Auth::user()->permission_level <= 1)
        {
            $users = User::all(); //fetch all users from DB
            return view('blog.admin', [
                    'users' => $users,
                ]); //returns the admin panel
        }
        else
        {
            return back(); // redirects the user back to the last page if the required permission level is not met
        }
    }

    public function edituser(User $user, BlogPost $blogPost)
    {
        if(Auth::check() && Auth::user()->permission_level <= 1)
        {
            $posts = BlogPost::all()->where('user_id',$user->id);  

            return view('blog.edituser', [
                    'user' => $user,
                    'posts' => $posts,
                ]); //returns the user info editing view
        }
        else
        {
            return back(); // redirects the user back to the last page if the required permission level is not met
        }
    }
    
    public function updateuser(Request $request, User $user)
    {
        $user->update([
            'name'=>$request->name,
            'email'=>$request->email
        ]);

        return redirect('/admin');
    }

    public function deleteuser(Request $request)
    {
        User::where('id',$request->user_id)->delete();

        return redirect('/admin');
    }
}
